package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sofka.jovenes_creativos.models.User;

import io.realm.Realm;

public class ConfirmActivity extends AppCompatActivity {

    private User user;
    private TextView etName;
    private TextView etDescription;
    private TextView etQuantity;
    private TextView etNumber;

    private Button btnConfirm;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        realm = Realm.getDefaultInstance();
        loadToolbar();
        loadViews();

        user = (User) getIntent().getSerializableExtra("user");
        loadUser();
    }

    private void seeUsers() {
        Intent intent = new Intent(this, UsersActivity.class);
        startActivity(intent);
    }

    private void loadUser() {
        etName.setText(user.getName());
        etDescription.setText(user.getDescription());
        etQuantity.setText(user.getQuantity());
        etNumber.setText(user.getNumber());

    }

    private void loadViews() {
            etName = findViewById(R.id.et_name);
            etDescription = findViewById(R.id.et_description);
            etQuantity = findViewById(R.id.et_quantity);
            etNumber = findViewById(R.id.et_number);
            btnConfirm = findViewById(R.id.btn_confirm);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmUser();
                seeUsers();
            }
        });
    }

    private void confirmUser() {
        Toast.makeText(this, "Producto confirmado", Toast.LENGTH_SHORT).show();
        addUserInDB(user);
    }

    private void addUserInDB(User user) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }



    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.confirmTitle);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
