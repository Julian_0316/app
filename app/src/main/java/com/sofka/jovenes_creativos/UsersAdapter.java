package com.sofka.jovenes_creativos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sofka.jovenes_creativos.models.User;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ItemViewHolder> {

    private Context context;
    private List<User> items;
    private UsersAdapterListener listener;

    public UsersAdapter(Context context, List<User> items, UsersAdapterListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public UsersAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.template_list_item, parent, false);
        return new UsersAdapter.ItemViewHolder(template);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersAdapter.ItemViewHolder holder, int position) {
        final User user = items.get(position);
        holder.tvName.setText(user.getName());
        holder.tvDescription.setText(user.getDescription());
        holder.tvQuantity.setText(user.getQuantity());
        holder.tvNumber.setText(user.getNumber());
        holder.tvValue.setText(user.getValor());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteUser(user.getId());
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.editUser(user.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateUsers(List<User> users) {
        this.items = users;
        notifyDataSetChanged();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDescription;
        private TextView tvQuantity;
        private TextView tvNumber;
        private TextView tvValue;
        private ImageView ivEdit;
        private ImageView ivDelete;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvQuantity = itemView.findViewById(R.id.tv_quantity);
            tvNumber = itemView.findViewById(R.id.tv_number);
            tvValue = itemView.findViewById(R.id.tv_valor);
            ivEdit = itemView.findViewById(R.id.iv_edit);
            ivDelete = itemView.findViewById(R.id.iv_delete);
        }
    }

    public interface UsersAdapterListener {

        boolean onQueryTextSubmit(String query);

        boolean onQueryTextChange(String newText);

        void deleteUser(String id);

        void editUser(String id);
    }
}
