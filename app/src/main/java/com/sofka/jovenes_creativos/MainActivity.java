package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.sofka.jovenes_creativos.models.User;

import java.util.UUID;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etQuantity;
    private EditText etNumber;
    private Button btnSave;
    private Button btnReject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadToolbar();
        loadViews();
        setupOnClicks();
        onSupportNavigateUp();

    }

    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.add_product);

        }
    }

        private void loadViews() {
        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        etQuantity = findViewById(R.id.et_quantity);
        etNumber = findViewById(R.id.et_number);
        btnReject = findViewById(R.id.btn_reject);
        btnSave = findViewById(R.id.btn_save);

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeUsers();
            }
        });

    }


    private void seeUsers() {
        Intent intent = new Intent(this, UsersActivity.class);
        startActivity(intent);
    }

    public void save(View view) {
        String name = etName.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        String quantity = etQuantity.getText().toString().trim();
        String number = etNumber.getText().toString().trim();

        boolean isSuccess = true;

        if (name.isEmpty()) {
            String message = getString(R.string.required);
            etName.setError(message);
            isSuccess = false;
        }
        if (description.isEmpty()) {
            etDescription.setError(getString(R.string.required));
            isSuccess = false;
        }
        if (quantity.isEmpty()) {
            etQuantity.setError(getString(R.string.required));
            showAlert();
            isSuccess = false;
        }
        if (number.isEmpty()) {
            etNumber.setError(getString(R.string.required));
            isSuccess = false;
        }

        if (isSuccess) {
            String id = UUID.randomUUID().toString();
            User user = new User(id, name, description, quantity, number);
            openConfirmActivity(user);
        }

    }

    private void openConfirmActivity(User user) {
        Intent intent = new Intent(this, ConfirmActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
        startActivityForResult(intent, 100);
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.error_title);
        builder.setMessage(R.string.camp_required_message);
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
    }

    private void setupOnClicks() {
        View.OnClickListener onClick;
        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(v);
            }
        };
        btnSave.setOnClickListener(onClick);
    }
}
