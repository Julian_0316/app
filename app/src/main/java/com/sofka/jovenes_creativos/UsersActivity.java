package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sofka.jovenes_creativos.models.User;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import android.widget.SearchView;

import io.realm.Realm;

public class UsersActivity extends AppCompatActivity implements UsersAdapter.UsersAdapterListener,SearchView.OnQueryTextListener  {

    private RecyclerView recyclerView;
    private Realm realm;
    private UsersAdapter usersAdapter;
    private TextView tvValue;
    private SearchView editsearch;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        realm = Realm.getDefaultInstance();
        recyclerView = findViewById(R.id.recycler_view);
        tvValue = findViewById(R.id.tv_valor);
        loadToolbar();
        loadUsers();

        editsearch = findViewById(R.id.search_view);
        editsearch.setOnQueryTextListener(this);
        setupOnClicks();

    }

    private void setupOnClicks() {
        floatingActionButton = findViewById(R.id.floatingActionButton);
        View.OnClickListener onClick;
        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeCreateUser();
            }
        };
        floatingActionButton.setOnClickListener(onClick);
    }

    private void seeCreateUser(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<User> users = searchUserByName(newText);
        usersAdapter = new UsersAdapter(this, users,this);
        recyclerView.setAdapter(usersAdapter);
        return false;
    }

    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.buys_list);

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateUsers();
        List<User> users = getUsers();
        setTotal(users);
    }

    private void loadUsers() {
        List<User> users = getUsers();
        setTotal(users);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        usersAdapter = new UsersAdapter(this, users,this);
        recyclerView.setAdapter(usersAdapter);

    }

    private List<User> searchUserByName(String input){
        List<User> users = realm.where(User.class).contains("name", input).findAll();
        return users;
    }

    private List<User> getUsers() {
        List<User> users = realm.where(User.class).findAll();
        return users;
    }

    @Override
    public void deleteUser(String id) {
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("id", id).findFirst();
        if (user != null) {
            user.deleteFromRealm();
        }
        realm.commitTransaction();

        updateUsers();
        List<User> users = getUsers();
        setTotal(users);
    }

    @Override
    public void editUser(String id) {
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra("userId", id);
        startActivity(intent);


    }

    private void setTotal(List<User> users){
        int sum = 0;
        for (User item : users) {
            if(!item.getValor().isEmpty()){
                sum += Integer.parseInt(item.getValor());
            }
        }

        NumberFormat nf = NumberFormat.getCurrencyInstance();
        String currency = nf.format(sum);

        tvValue.setText(currency);
    }


    private void updateUsers() {
        usersAdapter.updateUsers(getUsers());
        List<User> users = getUsers();
        setTotal(users);

    }
}
