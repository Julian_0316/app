package com.sofka.jovenes_creativos.api;

import com.sofka.jovenes_creativos.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UsersService {

    @POST("users")
    Call<User> createUser(@Body User user);

    @GET("users")
    Call<List<User>> getUsers();
}
