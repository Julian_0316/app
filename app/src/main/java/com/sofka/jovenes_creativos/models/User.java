package com.sofka.jovenes_creativos.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String name;
    private String description;
    private String quantity;
    private String number;

    public User() {
    }

    public User(String id, String name, String description, String quantity, String number) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getValor() {
        int c = 0, p = 0;
        if(!quantity.isEmpty()){
            c = Integer.parseInt(quantity);
        }
        if(!number.isEmpty()){
            p = Integer.parseInt(number);
        }
        return String.valueOf(c * p);
    }
}

