package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sofka.jovenes_creativos.models.User;

import io.realm.Realm;

public class EditUserActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etNumber;
    private Button btnSave;

    private Realm realm;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        realm = Realm.getDefaultInstance();
        loadViews();
        id = getIntent().getStringExtra("userId");
        loadUser(id);
    }

    private void loadUser(String id) {
        User user = realm.where(User.class).equalTo("id", id).findFirst();
        if (user != null) {
            etName.setText(user.getName());
            etLastName.setText(user.getLastName());
            etEmail.setText(user.getEmail());
            etNumber.setText(user.getNumber());
        }
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etLastName = findViewById(R.id.et_lastname);
        etEmail = findViewById(R.id.et_email);
        etNumber = findViewById(R.id.et_number);
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser();
            }
        });
    }

    private void updateUser() {
        String name = etName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String number = etNumber.getText().toString();
        User user = new User(id, name, lastName, email, number);
        updateUserInDB(user);
        finish();
    }

    private void updateUserInDB(User user) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }
}
