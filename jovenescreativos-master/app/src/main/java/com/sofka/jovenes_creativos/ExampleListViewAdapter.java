package com.sofka.jovenes_creativos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ExampleListViewAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> items;

    public ExampleListViewAdapter(@NonNull Context context, List<String> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.template_list_item, parent, false);
        String name = items.get(position);

        TextView tvName = template.findViewById(R.id.tv_item_name);
        tvName.setText(name);

        return template;
    }
}
