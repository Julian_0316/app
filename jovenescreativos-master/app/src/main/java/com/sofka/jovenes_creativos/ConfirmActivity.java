package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sofka.jovenes_creativos.models.User;

import io.realm.Realm;

public class ConfirmActivity extends AppCompatActivity {

    private User user;
    private TextView tvName;
    private TextView tvLastName;
    private TextView tvEmail;
    private TextView tvNumber;

    private Button btnReject;
    private Button btnConfirm;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        realm = Realm.getDefaultInstance();
        loadToolbar();
        loadViews();

        user = (User) getIntent().getSerializableExtra("user");
        loadUser();
    }

    private void loadUser() {
        tvName.setText(user.getName());
        tvLastName.setText(user.getLastName());
        tvEmail.setText(user.getEmail());
        tvNumber.setText(user.getNumber());
    }

    private void loadViews() {
        tvName = findViewById(R.id.tv_name);
        tvLastName = findViewById(R.id.tv_lastname);
        tvEmail = findViewById(R.id.tv_email);
        tvNumber = findViewById(R.id.tv_number);
        btnReject = findViewById(R.id.btn_reject);
        btnConfirm = findViewById(R.id.btn_confirm);

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeUsers();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmUser();
            }
        });
    }

    private void confirmUser() {
        Toast.makeText(this, "Usuario confirmado", Toast.LENGTH_SHORT).show();
        addUserInDB(user);
    }

    private void addUserInDB(User user) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }


    private void seeUsers() {
        Intent intent = new Intent(this, UsersActivity.class);
        startActivity(intent);
    }

    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.confirmTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
