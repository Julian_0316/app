package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.sofka.jovenes_creativos.models.User;

import java.util.List;

import io.realm.Realm;

public class UsersActivity extends AppCompatActivity implements UsersAdapter.UsersAdapterListener {

    private RecyclerView recyclerView;
    private Realm realm;
    private UsersAdapter usersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        realm = Realm.getDefaultInstance();
        recyclerView = findViewById(R.id.recycler_view);
        loadUsers();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateUsers();
    }

    private void loadUsers() {
        List<User> users = getUsers();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        usersAdapter = new UsersAdapter(this, users,this);
        recyclerView.setAdapter(usersAdapter);
    }

    private List<User> getUsers() {
        List<User> users = realm.where(User.class).findAll();
        return users;
    }

    @Override
    public void deleteUser(String id) {
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("id", id).findFirst();
        if (user != null) {
            user.deleteFromRealm();
        }
        realm.commitTransaction();

        updateUsers();
    }

    @Override
    public void editUser(String id) {
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra("userId", id);
        startActivity(intent);
    }

    private void updateUsers() {
        usersAdapter.updateUsers(getUsers());
    }
}
