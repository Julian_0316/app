package com.sofka.jovenes_creativos;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.sofka.jovenes_creativos.models.User;

import java.util.UUID;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etNumber;
    private CheckBox cbActive;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadViews();
        setupOnClicks();
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etLastName = findViewById(R.id.et_lastname);
        etEmail = findViewById(R.id.et_email);
        etNumber = findViewById(R.id.et_number);
        btnSave = findViewById(R.id.btn_save);
    }

    public void save(View view) {
        String name = etName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String number = etNumber.getText().toString().trim();

        boolean isSuccess = true;

        if (name.isEmpty()) {
            String message = getString(R.string.required);
            etName.setError(message);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            isSuccess = false;
        }
        if (lastName.isEmpty()) {
            etLastName.setError(getString(R.string.required));
            isSuccess = false;
        }
        if (email.isEmpty()) {
            etEmail.setError(getString(R.string.required));
            showAlert();
            isSuccess = false;
        }
        if (number.isEmpty()) {
            etNumber.setError(getString(R.string.required));
            isSuccess = false;
        }

        if (!cbActive.isChecked()) {
            cbActive.setError(getString(R.string.required));
            isSuccess = false;
        }

        if (isSuccess) {
            String id = UUID.randomUUID().toString();
            User user = new User(id, name, lastName, email, number);
            openConfirmActivity(user);
        }

    }

    private void openConfirmActivity(User user) {
        Intent intent = new Intent(this, ConfirmActivity.class);
        intent.putExtra("user", user);
//        startActivity(intent);
        startActivityForResult(intent, 100);
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.error_title);
        builder.setMessage(R.string.email_required_message);
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
    }

    private void setupOnClicks() {
        View.OnClickListener onClick;
        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(v);
            }
        };
        btnSave.setOnClickListener(onClick);
    }
}
